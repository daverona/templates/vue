# daverona/templates/vue

## Setup

```bash
# Create Vue project.
yarn create vuetify
# ? Project name: frontend
# ? Which preset would you like to install? Custom (Choose your features)
# ? Use TypeScript? Yes
# ? Use Vue Router? Yes
# ? Use Pinia? Yes
# ? Use ESLint? Yes
# ? Would you like to install dependencies with yarn, npm, or pnpm? yarn
cd frontend
rm -rf src/views/HomeView.vue src/assets/logo.png
touch src/views/.gitkeep

# Change package management to yarn pnp.
cd frontend
rm -rf node_modules yarn.lock
cat << EOT | tee -a .gitignore

# yarn pnp (instead of npm or pnpm)
.yarn/*
!.yarn/patches
!.yarn/plugins
!.yarn/releases
!.yarn/sdks
!.yarn/versions
# uncomment the following line if you want yarn pnp with zero-install
#!.yarn/cache
# uncomment the following line if you want yarn pnp without zero-install
.pnp.*
EOT
yarn set version berry
yarn install

# Install a plugin which installs @types counterparts for JavaScript packages automatically
yarn plugin import typescript

# Install a sdk for VisualStudio Code
code --install-extension arcanis.vscode-zipfs
yarn dlx @yarnpkg/sdks vscode
```
## References
